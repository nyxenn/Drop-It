import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalDetailsPage } from './modal-details.page';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [ModalDetailsPage],
    entryComponents: [ModalDetailsPage]
})
export class ModalDetailsPageModule {}
